import MainActionResult from "../MainActionResult.js";

export default class MainActionResultFourth extends MainActionResult {
  constructor() {
    super("MainActionResultFourth", "Final");
  }
  init(data) {
    this.data = data;
  }
  preload() {
    this.load.image("choice-smth-small", "src/img/choice-icon/first-step.png");
  }

  create() {
    this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(300, 500, "woman-mainScene-fourth").setScale(0.9);
    this.add.image(300, 30, "third-step");
    this.add.image(495, 30, "choice-smth-small");

    this.btnToggleScene("btn-place-left", 150);
    this.btnToggleScene("btn-place-right", 450);
    this.getNextScreen("Final");
  }
}
