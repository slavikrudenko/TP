import { whatToChoose, textChooseStyles } from "../data.js";
import MainAction from "../MainAction.js";

export default class MainActionFourth extends MainAction {
  constructor() {
    super("MainActionFourth", "MainActionResultFourth");
  }
  init(data) {
    this.data = data;
  }

  preload() {
    this.load.image("btn-place-left", "src/img/button/button-four-left.png");
    this.load.image("btn-place-right", "src/img/button/button-four-right.png");
  }

  create() {
    this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(0, 0);
    this.add.image(300, 500, "woman-mainScene-fourth").setScale(0.9);
    this.add.image(300, 30, "choice-smth").setScale(0.5);

    this.add.text(200, 16, whatToChoose[3], textChooseStyles);

    this.btnToggleScene(
      "btn-place-left",
      "place",
      "2",
      150,
      "MainActionResultFourth",
      this.data
    );
    this.btnToggleScene(
      "btn-place-right",
      "place",
      "1",
      450,
      "MainActionResultFourth",
      this.data
    );
    this.setCursor();
  }
  update() {}
}
