export default class MainAction extends Phaser.Scene {
  constructor(config) {
    super(config);
  }

  btnToggleScene(btn, key, value, positionY, nextScreen, objData) {
    const box = this.add.image(positionY, 730, "white-box").setScale(0.8);
    box.setInteractive();

    box.on("pointerover", () => {
      this.boxBorder = this.add
        .image(positionY, 730, "white-box-border")
        .setScale(0.4);
    });

    box.on("pointerout", () => {
      this.boxBorder.destroy();
    });

    this.add.image(positionY, 730, btn).setScale(0.8);

    box.on("pointerdown", () => {
      let btnPosition = btn.split("-")[2];

      this.scene.start(nextScreen, {
        girlStyle: { ...objData.girlStyle, [key]: value },
        btnPosition: btnPosition,
      });
    });
  }

  setCursor(background) {
    this.cursor = this.add.image(150, 750, "cursor");
    this.cursor.setDepth(1);

    const waitPointer = this.tweens.add({
      targets: this.cursor,
      x: 450,
      y: 750,
      delay: 2000,
      duration: 2000,
      repeat: 1000,
      yoyo: true,
    });

    this.input.on("pointermove", (pointer) => {
      waitPointer.stop();
      this.tweens.add({
        targets: background,
        alpha: 0.9,
        duration: 500,
      });

      this.tweens.add({
        targets: this.cursor,
        x: pointer.worldX,
        y: pointer.worldY,
        duration: 10,
      });
    });

    var timeout;
    document.onmousemove = () => {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        this.scene.restart();
      }, 1000);
    };

    this.input.setDefaultCursor("none");
  }
}
