import Intro from "./Screens/Intro.js";
import Tutorial from "./Screens/Tutorial.js";
import MainActionFirst from "./Screens/GamePlay/GamePlay-01/MainActionFirst.js";
import MainActionResultFirst from "./Screens/GamePlay/GamePlay-01/MainActionResultFirst.js";
import MainActionSecond from "./Screens/GamePlay/GamePlay-02/MainActionSecond.js";
import MainActionResultSecond from "./Screens/GamePlay/GamePlay-02/MainActionResultSecond.js";
import MainActionThird from "./Screens/GamePlay/GamePlay-03/MainActionThird.js";
import MainActionResultThird from "./Screens/GamePlay/GamePlay-03/MainActionResultThird.js";
import MainActionFourth from "./Screens/GamePlay/GamePlay-04/MainActionFourth.js";
import MainActionResultFourth from "./Screens/GamePlay/GamePlay-04/MainActionResultFourth.js";
import Final from "./Screens/AmazingScreen .js";
import PlayAgain from "./Screens/EndCard.js";

const config = {
  type: Phaser.AUTO,
  width: 600,
  height: 850,
  scene: [
    Intro,
    Tutorial,
    MainActionFirst,
    MainActionResultFirst,
    MainActionSecond,
    MainActionResultSecond,
    MainActionThird,
    MainActionResultThird,
    MainActionFourth,
    MainActionResultFourth,
    Final,
    PlayAgain,
  ],
};

const resize = () => {
  var canvas = document.querySelector("canvas");
  var windowWidth = window.innerWidth;
  var windowHeight = window.innerHeight;
  var windowRatio = windowWidth / windowHeight;
  var gameRatio = game.config.width / game.config.height;

  if (windowRatio < gameRatio) {
    canvas.style.width = windowWidth + "px";
    canvas.style.height = windowWidth / gameRatio + "px";
  } else {
    canvas.style.width = windowHeight * gameRatio + "px";
    canvas.style.height = windowHeight + "px";
  }
};

var game = new Phaser.Game(config);
resize();
