import MainActionResult from "../MainActionResult.js";
export default class MainActionResultFirst extends MainActionResult {
  constructor() {
    super("MainActionResultFirst", "MainActionSecond");
  }
  init(data) {
    this.data = data;
  }
  preload() {
    this.load.image(
      "woman-mainScene-second",
      `src/img/persons/woman-${this.data.girlStyle.dress}.png`
    );

    this.load.image("first-step", "src/img/choice-icon/1step.png");
  }
  create() {
    this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(300, 500, "woman-mainScene-second").setScale(0.9);
    this.add.image(300, 30, "first-step").setScale(1);

    this.btnToggleScene("btn-first-left", 150);
    this.btnToggleScene("btn-first-right", 450);
    this.getNextScreen("MainActionSecond");
  }
}
