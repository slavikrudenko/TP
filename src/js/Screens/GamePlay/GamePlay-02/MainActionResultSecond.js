import MainActionResult from "../MainActionResult.js";
export default class MainActionResultSecond extends MainActionResult {
  constructor() {
    super("MainActionResultSecond", "MainActionThird");
  }
  init(data) {
    this.data = data;
  }
  preload() {
    this.load.image(
      "woman-mainScene-third",
      `src/img/persons/woman-${this.data.girlStyle.dress}-${this.data.girlStyle.bag}.png`
    );

    this.load.image("second-step", "src/img/choice-icon/2step.png");
  }

  create() {
    this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(300, 500, "woman-mainScene-third").setScale(0.9);
    this.add.image(300, 30, "second-step");

    this.btnToggleScene("btn-second-left", 150);
    this.btnToggleScene("btn-second-right", 450);
    this.getNextScreen("MainActionThird");
  }
}
