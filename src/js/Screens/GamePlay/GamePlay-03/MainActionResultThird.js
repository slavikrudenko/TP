import MainActionResult from "../MainActionResult.js";
export default class MainActionResultThird extends MainActionResult {
  constructor() {
    super("MainActionResultThird", "MainActionFourth");
  }
  init(data) {
    this.data = data;
  }
  preload() {
    this.load.image(
      "woman-mainScene-fourth",
      `src/img/persons/woman-${this.data.girlStyle.dress}-${this.data.girlStyle.bag}-${this.data.girlStyle.accessory}.png`
    );
    this.load.image("third-step", "src/img/choice-icon/3step.png");
  }

  create() {
    this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(300, 500, "woman-mainScene-fourth").setScale(0.9);
    this.add.image(300, 30, "third-step");

    this.btnToggleScene("btn-accessory-left", 150);
    if (this.data.girlStyle.dress !== "white") {
      this.btnToggleScene("btn-accessory-right-blue", 450);
    } else {
      this.btnToggleScene("btn-accessory-right", 450);
    }

    this.getNextScreen("MainActionFourth");
  }
}
