export default class PlayAgain extends Phaser.Scene {
  constructor() {
    super("PlayAgain", "MainActionFirst");
  }
  init(data) {
    this.data = data;
  }

  preload() {
    this.load.image("playNow", "src/img/button/PlayNow.png");
  }

  create() {
    this.add.image(150, 650, "backgroundFinal").setScale(0.7);
    this.add.image(400, 500, "man-default").setScale(0.6);
    this.add.image(200, 500, "woman-mainScene-fourth").setScale(0.9);
    const playAgain = this.add.image(300, 800, "playNow").setScale(0.9);
    playAgain.setInteractive();

    playAgain.on("pointerdown", () => {
      this.scene.start("MainActionFirst", {});
    });

    //add cursor
    this.cursor = this.add.image(-100, -100, "cursor");
    this.cursor.setDepth(1);

    this.input.on("pointermove", (pointer) => {
      this.tweens.add({
        targets: this.cursor,
        x: pointer.worldX,
        y: pointer.worldY,
        duration: 10,
      });
    });
    this.input.setDefaultCursor("none");
  }
  update() {}
}
