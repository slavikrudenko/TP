export default class Intro extends Phaser.Scene {
  constructor() {
    super("Intro", "Tutorial");
  }
  init(data) {}
  preload() {
    //background
    this.load.image("background1", "src/img/background/background1.jpg");

    //persons
    this.load.image("man-default", "src/img/persons/man-default.png");
    this.load.image("woman-default", "src/img/persons/woman-default.png");
    //dialog
    this.load.image("man-talk", "src/img/dialogs/man-talk.png");
    this.load.image("woman-talk", "src/img/dialogs/woman-talk.png");
  }
  create() {
    this.background = this.add
      .image(200, 450, "background1")
      .setScale(0.5)
      .setAlpha(0.4);

    this.persons = {
      man: this.add.image(300, 450, "man-default").setScale(0.7),
      woman: this.add.image(-230, 480, "woman-default").setScale(0.95),
    };

    this.dialoguePhrase = {
      man: this.add.image(200, 550, "man-talk").setScale(0.0),
      woman: this.add.image(200, 550, "woman-talk").setScale(0.0),
    };

    //dialog starting

    this.tweens.add({
      targets: this.dialoguePhrase.man,
      x: 300,
      duration: 300,
      loop: false,
      scale: 0.3,
      delay: 500,
    });
    //man and phrase hide down
    this.tweens.add({
      targets: this.persons.man,
      x: 900,
      duration: 500,
      loop: false,
      delay: 1000,
    });

    this.tweens.add({
      targets: this.dialoguePhrase.man,
      x: 200,
      duration: 300,
      loop: false,
      delay: 1000,
      scale: 0.0,
    });

    //woman and phrase show up
    this.tweens.add({
      targets: this.persons.woman,
      x: 300,
      duration: 500,
      loop: false,
      delay: 1000,
    });

    this.tweens.add({
      targets: this.dialoguePhrase.woman,
      x: 300,
      duration: 300,
      loop: false,
      scale: 0.3,
      delay: 1000,
    });

    //man phrase hide down
    this.tweens.add({
      targets: this.dialoguePhrase.woman,
      x: 200,
      duration: 300,
      loop: false,
      delay: 1750,
      scale: 0.0,
    });

    this.tweens.add({
      targets: this.background,
      duration: 300,
      loop: false,
      delay: 2050,
      alpha: 0.8,
    });

    this.time.addEvent({
      delay: 2350,
      loop: false,
      callback: () => {
        this.scene.start("Tutorial", {});
      },
    });
    this.input.setDefaultCursor("none");
  }
}
