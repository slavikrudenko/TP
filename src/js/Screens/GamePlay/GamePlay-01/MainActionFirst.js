import { whatToChoose, textChooseStyles } from "../data.js";
import MainAction from "../MainAction.js";

export default class MainActionFirst extends MainAction {
  constructor() {
    super("MainActionFirst", "PlayAgain");
  }

  preload(data) {
    this.data = data;
  }

  create() {
    const background = this.add
      .image(200, 450, "background1")
      .setScale(0.5)
      .setAlpha(0.9);

    this.add.image(300, 520, "woman-mainScene-first").setScale(0.9);
    this.add.image(300, 30, "choice-smth").setScale(0.5);

    this.add.text(200, 16, whatToChoose[0], textChooseStyles);

    this.btnToggleScene(
      "btn-first-left",
      "dress",
      "pink",
      150,
      "MainActionResultFirst",
      {}
    );
    this.btnToggleScene(
      "btn-first-right",
      "dress",
      "white",
      450,
      "MainActionResultFirst",
      {}
    );
    this.setCursor(background);
  }
  update() {}
}
