export default class MainActionResult extends Phaser.Scene {
  constructor(config) {
    super(config);
  }
  btnToggleScene(btn, positionY) {
    this.add.image(positionY, 730, "white-box").setScale(0.8);
    if (btn.split("-")[2] === this.data.btnPosition) {
      this.add.image(positionY, 730, "white-box-border").setScale(0.4);
    }
    this.add.image(positionY, 730, btn).setScale(0.8);
  }

  getSuccessLevel(choiceLevelStep) {
    this.tweens.add({
      targets: choiceLevelStep,
      x: 131 * 2,
      scale: 1,
      duration: 1000,
    });
  }

  getNextScreen(nextScreen) {
    this.scene.scene.time.addEvent({
      delay: 1500,
      callback: () => {
        this.scene.start(nextScreen, {
          ...this.data,
        });
      },
    });
  }
}
