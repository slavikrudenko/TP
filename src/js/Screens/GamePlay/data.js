const whatToChoose = [
  "Choose your dress",
  "Choose your bag",
  "Choose your accessory",
  "Choose your place",
];

const textChooseStyles = {
  fontFamily: "Nunito Sans",
  fontStyle: "normal",
  fontWeight: "700",
  fontSize: "24px",
  lineHeight: "33px",
  color: "#FFFFFF",
};

export { whatToChoose, textChooseStyles };
