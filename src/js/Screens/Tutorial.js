import { whatToChoose, textChooseStyles } from "./GamePlay/data.js";

export default class Tutorial extends Phaser.Scene {
  constructor() {
    super("Tutorial", "MainActionFirst");
  }

  preload() {
    this.load.image("background1", "src/img/background/background1.jpg");
    this.load.image(
      "woman-mainScene-first",
      "src/img/persons/woman-mainScene-first.png"
    );
    this.load.image("choice-smth", "src/img/choice-icon/up-text-window.png");
    this.load.image("btn-first-left", "src/img/button/button-first-left.png");
    this.load.image("btn-first-right", "src/img/button/button-first-right.png");
    this.load.image("cursor", "src/img/cursor-min.png");
    this.load.image("white-box", "src/img/button/white-box.png");
    this.load.image("white-box-border", "src/img/button/white-box-border.png");
  }

  create() {
    const background = this.add
      .image(200, 450, "background1")
      .setScale(0.5)
      .setAlpha(0.4);

    const woman = this.add.image(300, 450, "woman-mainScene-first");

    this.add.image(300, 30, "choice-smth").setScale(0.5);

    this.tweens.add({
      targets: woman,
      duration: 900,
      loop: false,
      scale: 0.9,
      y: 520,
    });

    //add choice Text

    this.add.text(200, 16, whatToChoose[0], textChooseStyles);

    //add btn
    const btnToggleScene = (btn, positionY) => {
      this.add.image(positionY, 730, "white-box").setScale(0.8);
      this.add.image(positionY, 730, btn).setScale(0.8);
    };

    btnToggleScene("btn-first-left", 150);
    btnToggleScene("btn-first-right", 450);

    //add cursor

    this.cursor = this.add.image(-100, -100, "cursor");
    this.cursor.setDepth(1);

    this.input.on("pointermove", (pointer) => {
      this.tweens.add({
        targets: woman,
        duration: 500,
        loop: false,
        scale: 0.9,
        y: 520,
      });

      this.tweens.add({
        targets: background,
        alpha: 0.9,
        duration: 500,
        callback: () => {
          this.scene.start("MainActionFirst", {});
        },
      });

      this.tweens.add({
        targets: this.cursor,
        x: pointer.worldX,
        y: pointer.worldY,
        duration: 10,
      });
    });
    this.input.setDefaultCursor("none");
  }
  update() {}
}
