export default class Final extends Phaser.Scene {
  constructor() {
    super("Final", "PlayAgain");
  }
  init(data) {
    this.data = data;
  }

  preload() {
    this.load.image(
      "backgroundFinal",
      `src/img/background/background${+this.data.girlStyle.place + 1}.jpg`
    );
    this.load.image("dialogFinal", "src/img/dialogs/final.png");
  }

  create() {
    this.add.image(150, 650, "backgroundFinal").setScale(0.7);
    this.add.image(400, 500, "man-default").setScale(0.6);
    this.add.image(200, 500, "woman-mainScene-fourth").setScale(0.9);
    this.add.image(300, 500, "dialogFinal");

    this.time.addEvent({
      delay: 2000,
      loop: false,
      callback: () => {
        this.scene.start("PlayAgain", {});
      },
    });
  }
  update() {}
}
