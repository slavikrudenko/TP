import { whatToChoose, textChooseStyles } from "../data.js";
import MainAction from "../MainAction.js";

export default class MainActionThird extends MainAction {
  constructor() {
    super("MainActionThird", "MainActionResultThird");
  }
  init(data) {
    this.data = data;
  }

  preload() {
    this.load.image(
      "btn-accessory-left",
      "src/img/button/button-three-left.png"
    );
    this.load.image(
      "btn-accessory-right",
      "src/img/button/button-three-right.png"
    );
    this.load.image("btn-accessory-right-blue", "src/img/button/neck-blue.png");
  }

  create() {
    const background = this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(300, 500, "woman-mainScene-third").setScale(0.9);
    this.add.image(300, 30, "choice-smth").setScale(0.5);

    this.add.text(200, 16, whatToChoose[2], textChooseStyles);

    this.btnToggleScene(
      "btn-accessory-left",
      "accessory",
      "glasses",
      150,
      "MainActionResultThird",
      this.data
    );

    if (this.data.girlStyle.dress === "white") {
      this.btnToggleScene(
        "btn-accessory-right",
        "accessory",
        "neck",
        450,
        "MainActionResultThird",
        this.data
      );
    } else {
      this.btnToggleScene(
        "btn-accessory-right-blue",
        "accessory",
        "neck",
        450,
        "MainActionResultThird",
        this.data
      );
    }
    this.setCursor(background);
  }
  update() {}
}
