import { whatToChoose, textChooseStyles } from "../data.js";
import MainAction from "../MainAction.js";

export default class MainActionSecond extends MainAction {
  constructor() {
    super("MainActionSecond", "MainActionResultSecond");
  }
  init(data) {
    this.data = data;
  }
  preload() {
    this.load.image("btn-second-left", "src/img/button/button-two-left.png");
    this.load.image("btn-second-right", "src/img/button/button-two-right.png");
  }
  create() {
    const background = this.add.image(200, 450, "background1").setScale(0.5);
    this.add.image(0, 0);
    this.add.image(300, 500, "woman-mainScene-second").setScale(0.9);
    this.add.image(300, 30, "choice-smth").setScale(0.5);

    this.add.text(200, 16, whatToChoose[1], textChooseStyles);

    this.btnToggleScene(
      "btn-second-left",
      "bag",
      "yellow",
      150,
      "MainActionResultSecond",
      this.data
    );
    this.btnToggleScene(
      "btn-second-right",
      "bag",
      "blue",
      450,
      "MainActionResultSecond",
      this.data
    );
    this.setCursor(background);
  }
}
